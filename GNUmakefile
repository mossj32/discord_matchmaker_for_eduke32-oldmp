ifndef PLATFORM
    uname:=$(strip $(shell uname -s))
    PLATFORM=UNKNOWN
    ifeq ($(findstring Linux,$(uname)),Linux)
        PLATFORM=LINUX
    endif
    ifeq ($(findstring BSD,$(uname)),BSD)
        PLATFORM=BSD
    endif
    ifeq ($(findstring MINGW,$(uname)),MINGW)
        PLATFORM=WINDOWS
    endif
    ifeq ($(findstring Darwin,$(uname)),Darwin)
        PLATFORM=DARWIN
    endif
    ifeq ($(findstring BeOS,$(uname)),BeOS)
        PLATFORM=BEOS
    endif
    ifeq ($(findstring skyos,$(uname)),skyos)
        PLATFORM=SKYOS
    endif
    ifeq ($(findstring QNX,$(uname)),QNX)
        PLATFORM=QNX
    endif
    ifeq ($(findstring SunOS,$(uname)),SunOS)
        PLATFORM=SUNOS
    endif
    ifeq ($(findstring syllable,$(uname)),syllable)
        PLATFORM=SYLLABLE
    endif
endif

# FIXME - May exclusively use g++ after a minor correction to minisoap.c
# (void* to char* conversion)
DEBUG?=1
RC=windres
CC=gcc
CXX=g++
CFLAGS=-g -Isrc/enet/include
CXXFLAGS=-std=c++11 -g -Isrc/enet/include -Isrc/libipify -Isrc
LDFLAGS=-static-libstdc++ -static-libgcc

MATCHMAKER_OBJS=NetLauncher.o \
                NetPackets.o \
                Router.o \
                usefulfuncs.o \
                profport.o \
                WinDump.o \
                downloader.o

MINIUPNPC_OBJS=connecthostport.o \
               igd_desc_parse.o \
               minisoap.o \
               minissdpc.o \
               miniupnpc.o \
               miniwget.o \
               minixml.o \
               portlistingparse.o \
               receivedata.o \
               upnpcommands.o \
               upnpdev.o \
               upnperrors.o \
               upnpreplyparse.o
               
ENET_OBJS=callbacks.o \
          compress.o \
          host.o \
          list.o \
          packet.o \
          peer.o \
          protocol.o
          
IPIFY_OBJS=ipify.o
          
ifeq ($(PLATFORM),WINDOWS)
OBJDIR=obj_win

MATCHMAKER_OBJS+=NetLauncher.rco
ENET_OBJS+=win32.o

LDFLAGS+=-lmingwex -lwsock32 -lws2_32 -lshlwapi -lwinmm -liphlpapi -lurlmon -static -lpthread
CFLAGS+=-DMINIUPNP_STATICLIB -DWIN32
CXXFLAGS+=-DMINIUPNP_STATICLIB -DWIN32

BINARY_NAME=NetLauncher.exe 
else
OBJDIR=obj_linux

# -pthread is required if we do static linking
ENET_OBJS+=unix.o
LDFLAGS+=-lpthread

BINARY_NAME=NetLauncher-linux64
endif

ifeq ($(DEBUG),0)
LDFLAGS+=--strip-debug
CFLAGS+=-O2
CXXFLAGS+=-O2
else
CFLAGS+=-g -DDEBUG -D_DEBUG
CXXFLAGS+=-g -DDEBUG -D_DEBUG
endif

BAREOBJS=$(MATCHMAKER_OBJS) $(MINIUPNPC_OBJS) $(ENET_OBJS) $(IPIFY_OBJS)
OBJS=$(addprefix $(OBJDIR)/,$(BAREOBJS))

.PHONY: $(BINARY_NAME) clean

$(BINARY_NAME): $(OBJS)
	$(CXX) $(OBJS) $(LDFLAGS) -o ./Bin/$@

clean:
	-rm -f ./Bin/$(BINARY_NAME) $(OBJS)

$(OBJDIR)/%.rco: %.rc
	$(RC) $< -o $@

$(OBJDIR)/%.o: src/%.cpp | obj
	$(CXX) -c $(CXXFLAGS) $< -o $@

$(OBJDIR)/%.o: src/libipify/%.cpp | obj
	$(CXX) -c $(CXXFLAGS) $< -o $@

$(OBJDIR)/%.o: src/miniupnpc/%.c | obj
	$(CC) -c $(CFLAGS) $< -o $@

$(OBJDIR)/%.o: src/enet/src/%.c | obj
	$(CC) -c $(CFLAGS) $< -o $@

obj:
	mkdir -p $(OBJDIR)