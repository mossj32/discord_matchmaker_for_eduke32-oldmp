#ifndef _USEFULFUNCS_
#define _USEFULFUNCS_

#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include <string>

typedef struct {
	int32_t x, y;
} vec2_t;

#define ARRAY_SIZE(arr) (sizeof(arr)/sizeof(arr[0]))

char* GetThisPath(char* dest, size_t destSize);
int prompt(char* line, size_t size);

// String Manipulation
void find_string(char* string, const char* pattern, char buffer[], int buflength, char stopchar);
char* strremove(char* str, const char* sub);
char* extract_between(const char* str, const char* p1, const char* p2);
char* stristr(const char* str1, const char* str2);
const char* Duke_ConvertColorCodes(char* out, const char* in);

bool isValidIpAddress(const char *ip);

// Console Manipulation
void gotoxy(int x, int y);
vec2_t getconsoledims();

#ifdef _WIN32
void colorprintf(const char *, ...);
char* convert(unsigned int, int);
void toClipboard(const std::string& s);
#define kbhit _kbhit
#define getch _getch
#else
#define colorprintf printf
bool kbhit();
char getch();
#endif

#endif