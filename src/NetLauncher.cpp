#ifdef _WIN32
#ifndef _CRT_SECURE_NO_WARNINGS
	#define _CRT_SECURE_NO_WARNINGS /* thanks Microsoft */
#endif
#ifndef _WINSOCK_DEPRECATED_NO_WARNINGS
	#define _WINSOCK_DEPRECATED_NO_WARNINGS
#endif
#define WIN32_LEAN_AND_MEAN

// Required Libraries
#pragma comment(lib, "user32.lib")
#pragma comment(lib, "ws2_32.lib") 
#pragma comment(lib, "shlwapi.lib")
#pragma comment(lib, "Iphlpapi.lib")
#pragma comment(lib, "urlmon.lib")
#pragma comment(lib, "winmm.lib")
#endif

#include "NetLauncher.h"
#include "NetPackets.h"
#include "downloader.h"

#include "ipify.h"
#include "profport.h"
#include "Router.h"

#ifdef _WIN32
#include "WinDump.h"
#endif

// ************
// MAIN PROGRAM
// ************
char myClientName[128];

char path[MAX_PATH];

char modList[MAX_MODS_COUNT][MAX_PATH];
int num_mods;

LobbyData lobby;
ClientData_t ClientData[MAXCLIENTS];
int myClientIndex;

int router_initialized = 0;
Router* router;

#ifdef _WIN32
PROCESS_INFORMATION procinfo;
STARTUPINFO info = { sizeof(info) };
#endif

void killme(int signal)
{
	Net_Kill(NETSTATUS_TERMINATED);

	enet_deinitialize();

	exit(0);
}

void Lobby_Reset()
{
	lobby.connected = false;
	lobby.partySize = 1;
	lobby.partyMax = 1;
	lobby.fraglimit = 0;
	lobby.starting = 0;
	lobby.modname[0] = 0;

	for (int32_t ALL_CLIENTS(i))
		ClientData[i].ready = (i == 0) ? true : false;
}

void PopulateModsList()
{
	char modPath[MAX_PATH];
	sprintf(modPath, "%s/" MODS_DIR, path);

	tinydir_dir dir;
	if (tinydir_open(&dir, modPath) != 0)
	{
		printf("Couldn't open mods dir.\n");
		return;
	}

	if (!dir.has_next)
	{
		printf("No mod files found.\n");
		return;
	}

	do
	{
		tinydir_file file;
		tinydir_readfile(&dir, &file);

		if (!file.is_dir &&
		    (!strcasecmp(file.extension, "grp") || !strcasecmp(file.extension, "pk3"))
		)
		{
			num_mods++;
			sprintf(modList[num_mods - 1], "%s", file.name);
		}
		tinydir_next(&dir);
	} while (dir.has_next && (num_mods < MAX_MODS_COUNT));

	tinydir_close(&dir);
}

int initRouter()
{
	printf("Initializing UPnP...\n");
	
	try
	{
		router = new Router(4000);
	}
	catch (std::runtime_error& ex)
	{
		printf("*** ERROR: %s\n", ex.what());
		Sleep(1000);
		return 0;
	}

	try
	{
		printf("Attempting UPnP Port Forward...\n");
		if (!router->setPortMapping(GAMEPORT, GAMEPORT, Router::MAP_UDP, "EDuke32"))
		{
			printf("Could not forward port.\n");
			Sleep(1000);
			return 0;
		}
	}
	catch (std::runtime_error& ex)
	{
		printf("*** ERROR: %s\n", ex.what());
		Sleep(1000);
		return 0;
	}

	return 1;
}

void StartGame(const char* ip)
{
	printf("\nStarting game.\n");
	Net_Kill(NETSTATUS_TERMINATED);

	if (!isServer && ip == NULL)
		return;

	// Gather lobby info, launch.
	char cmd_buffer[MAX_PATH];
	char* target = cmd_buffer;
	target += sprintf(target, EXE_PATH SPACER BASE_PARAMETERS SPACER);

	if (lobby.modname[0] != 0)
		target += sprintf(target, "-g " MODS_DIR "/%s" SPACER, lobby.modname);

	if (isServer)
	{
		if (lobby.usingAI)
			target += sprintf(target, "-q%d -a" SPACER, lobby.partyMax);

		target += sprintf(target, "-map E1L1.MAP -t23 -m -c1 -y%d" SPACER, lobby.fraglimit);

		if (lobby.partySize > 1)
			target += sprintf(target, "-net -n0:%d", lobby.partySize);
	}
	else
	{
		target += sprintf(target, "-net -n0 %s -p%d", ip, GAMEPORT + myClientIndex);
	}

#ifdef _WIN32
	system(cmd_buffer);

	/*
	int result = CreateProcess(EXE_PATH, cmd_buffer, NULL, NULL, FALSE, 0, NULL, NULL, &info, &procinfo);
	if (!result)
	{
		printf("Failed to launch NetDuke32!\n");
		return;
	}
	*/
#else
	if (access(EXE_PATH, X_OK) != 0)
	{
		printf("Can't find or execute %s! (access returned errno==%d)\n", EXE_PATH, errno);
		return;
	}

	int result = fork();
	if (result == 0) // child
	{
		// More-or-less what system() does (http://man7.org/linux/man-pages/man3/system.3.html)
		execl("/bin/sh", "sh", "-c", cmd_buffer, NULL);
		killme(0);
	}
#endif
}

bool CheckEveryoneReady()
{
	for (int32_t ALL_CONNECTED(i))
		if (!ClientData[i].ready)
			return false;

	return true;
}

void Server_LaunchAndWait()
{
	char cmd_buffer[8192];
	char ip_addr[16];
	
	if (router_initialized)
	{
		std::string ip = router->getWanAddress();
		snprintf(ip_addr, sizeof(ip_addr), "%s", ip.c_str());
	}
	else get_ip(ip_addr);

	//CLEAR_SCREEN;
	PrintBanner();

	if (router_initialized)
	{
		printf("UPnP: Enabled!\n");
		printf("Internal IP: %s\n", router->getLocalAddress().c_str());
	}
	else
	{
		char hostname[128];
		gethostname(hostname, sizeof(hostname));
		hostent *hp = gethostbyname(hostname);
		in_addr **addr;
		
		addr = (in_addr **)hp->h_addr_list;

		printf("UPnP: N/A - Might need to forward UDP port %d\n\n", GAMEPORT);
		printf("-=Internal IP List=-\n");
		while (*addr != NULL) {
			printf("%s\n", inet_ntoa(**(addr++)));
		}
		printf("\n");
	}

	printf("External IP: %s\n", (ip_addr[0] != 0) ? ip_addr : "Unknown! Offline, perhaps?");
	printf("Mod: %s\n", (lobby.modname[0] != 0) ? lobby.modname : "None");
	printf("Max Players: %d\n", lobby.partyMax);
	printf("Frag Limit: %d\n", lobby.fraglimit);

	printf("\n");

	if(Net_CreateHost(lobby.partyMax) == NULL)
		killme(0);
	else
		printf("Lobby creation successful!\n\n");

	// Copy name into ClientData array.
	sprintf(ClientData[0].name, "%s", myClientName);

	isServer = true;
	lobby.connected = true;

	printf("Waiting for players, press Y to launch early (needs at least 2 players)...\n");
	printf("Press B to fill remaining player count with bots. (No other players needed!)\n");
#ifdef _WIN32
	printf("TIP: You can press backslash (\\) to copy a netlaunch:// URL to the clipboard!\n");
#endif
	printf("\n");

	while (!lobby.starting)
	{
		Net_HandlePackets();

		if (lobby.partySize == lobby.partyMax && CheckEveryoneReady())
		{
			lobby.starting = true;
			break;
		}

		if (kbhit())
		{
			char ch = getch();

			if ((lobby.partySize > 1) && (ch == 'y') && CheckEveryoneReady())
			{
				lobby.starting = true;
				break;
			}
			else if ((ch == 'b') && CheckEveryoneReady())
			{
				lobby.starting = true;
				lobby.usingAI = true;
				break;
			}
			else if (ch == 'q')
			{
				printf("Q pressed. Quitting.\n");
				killme(0);
				break;
			}
			else if (ch == '\\' && (ip_addr[0] != 0))
			{
#ifdef _WIN32
				char url[36];
				snprintf(url, sizeof(url), "netlaunch://%s/", ip_addr);
				toClipboard(url);
				printf("Copied netlaunch URL (%s) to clipboard.\n", url);
#else
				printf("The netlaunch:// URL protocol is only supported on windows, sorry.\n");
#endif
			}
		}

		Sleep(10);
	}

	printf("Starting game.\n");

	if(lobby.partySize > 1)
		Net_SendStartGame();

	StartGame(NULL);
}

ENetAddress getAddress(const char* ip)
{
	ENetAddress address;
	bool notIP = false;
	bool invalid = false;

	printf("Getting ENet address for IP: %s\n\n", ip);

	int32_t len = strlen(ip);
	for (int32_t i = 0; i < len; i++)
	{
		if (!isdigit(ip[i]) && ip[i] != '.')
		{
			notIP = true;
			break;
		}
	}

	if (!notIP && !isValidIpAddress(ip))
		invalid = true;
	else
	{
		// Attempt to set the address.
		int result = enet_address_set_host(&address, ip);
		if (result < 0 || address.host == 0)
			invalid = true;
	}

	if (invalid)
	{
		printf("Invalid address, or error getting host!\n");
		return ENetAddress{ 0, 0 };
	}

	return address;
}

void Client_ConnectLoop(ENetAddress* address)
{
	if (address->host == 0)
		return;

reconnect:
	Lobby_Reset();

	isServer = false;
	//CLEAR_SCREEN;
	PrintBanner();

	if (Net_ConnectToHost(address) == NULL)
		return;
	
	NetStatus_t netStatus = NETSTATUS_OK;
	while (!lobby.starting)
	{
		netStatus = Net_HandlePackets();
		if (netStatus != NETSTATUS_OK)
			break;
		Sleep(10);
	}

	if (netStatus == NETSTATUS_FILEFAIL)
	{
		char yn[4];
		colorprintf(C_BOLD C_RED "Looks like you're missing the mod the host is using!\n" C_RESET);
		colorprintf(C_BOLD C_YELLOW "Please copy " C_CYAN "%s" C_YELLOW " to your " C_CYAN MODS_DIR C_YELLOW " subfolder!\n\n" C_RESET, lobby.modname);
		colorprintf(C_BOLD C_WHITE "You have three options:\n" C_RESET);
		colorprintf(C_BOLD C_GREEN "1. Retry connection (once you've copied the file!).\n" C_RESET);
		colorprintf(C_BOLD C_CYAN "2. Attempt download from shadowmavericks.com.\n" C_RESET);
		colorprintf(C_BOLD C_RED "3. Abort and close NetLauncher.\n" C_RESET);
		while (1)
		{
			if (!prompt(yn, sizeof(yn)))
				continue;

			if (yn[0] == '1')
				goto reconnect;

			if (yn[0] == '2')
			{
				if(DownloadModFile(lobby.modname))
					goto reconnect;
			}

			if (yn[0] == '3')
				break;
		}
	}
}

void Menu_AskMod()
{
	char count[4];

	PopulateModsList();

	if (!num_mods)
		return;

	printf("\n");
	for (int i = 0; i < num_mods; i++)
	{
		printf("%d. %s\n", i + 1, modList[i]);
	}

	printf("Which Mod? (1-%d, 0 for None)", num_mods);
	while (1)
	{
		if (!prompt(count, sizeof(count)))
			continue;

		if (atoi(count) >= 1 && atoi(count) <= num_mods)
		{
			int selected_mod = atoi(count);
			sprintf(lobby.modname, "%s", modList[selected_mod - 1]);
			printf("Mod set to %s.\n", lobby.modname);
			break;
		}
		if (atoi(count) == 0)
		{
			printf("No mods.\n");
			break;
		}
		else
		{
			printf("Invalid selection!\n");
			continue;
		}
	}
}


void Menu_AskManualIP(bool forced)
{
	char ip[253];

askAgain:
	printf("Enter IP Address:\n");
	fgets(ip, sizeof(ip), stdin);
	ip[strcspn(ip, "\n")] = 0;

	ENetAddress address = getAddress(ip);
	if (address.host == 0)
		goto askAgain;

	if (forced)
	{
		StartGame(ip);
		killme(0);
		return;
	}

	Client_ConnectLoop(&address);
}

void Menu_AskUPnP()
{
	char yn[4];
	printf("\nAttempt to forward ports with UPnP? (y/n)\nNOTE: This may, or may not work depending on your network setup!");
	while (1)
	{
		if (!prompt(yn, sizeof(yn)))
			continue;

		if (yn[0] == 'y')
		{
			router_initialized = initRouter();
			break;
		}

		if (yn[0] == 'n')
		{
			router_initialized = 0;
			break;
		}
	}
}

void Menu_AskPlayerCount()
{
	char count[4];

	printf("\nMax Players? (2-16)");
	while (1)
	{
		if (!prompt(count, sizeof(count)))
			continue;

		if (atoi(count) >= 2 && atoi(count) <= 16)
		{
			lobby.partyMax = atoi(count);
			printf("Player count set to %d.\n", lobby.partyMax);
			break;
		}
		else
		{
			printf("Invalid player count!\n");
			continue;
		}
	}
}

void Menu_AskFragLimit()
{
	char count[4];

	printf("\nFrag limit? (0-100)");
	while (1)
	{
		if (!prompt(count, sizeof(count)))
			continue;

		if (atoi(count) >= 1 && atoi(count) <= 100)
		{
			lobby.fraglimit = atoi(count);
			printf("Frag limit set to %d.\n", lobby.fraglimit);
			break;
		}
		else if (atoi(count) == 0)
		{
			lobby.fraglimit = 0;
			printf("No frag limit.\n");
			break;
		}
		else
		{
			printf("Invalid!\n");
			continue;
		}
	}
}

void Menu_HostGameSetup()
{
	Lobby_Reset();

	Menu_AskPlayerCount();
	Menu_AskFragLimit();
	Menu_AskMod();
	Menu_AskUPnP();
	Server_LaunchAndWait();
}

void Menu_Main()
{
	char yn[4];
restart_menu:
#ifdef _DEBUG
	colorprintf(C_BOLD C_RED "-=Debugging Enabled=- If you get a crash, send crash.log to Striker!\n" C_RESET);
#endif
	colorprintf("Current Directory: " C_BOLD C_CYAN "%s\n" C_RESET, path);
	colorprintf("\nUsername: " C_BOLD C_CYAN "%s\n\n" C_RESET, myClientName);
	printf("Select one:\n\n");

	colorprintf(C_BOLD C_YELLOW "1. " C_RESET "Host Game\n");
	colorprintf(C_BOLD C_YELLOW "2. " C_RESET "Join via IP\n");
	colorprintf(C_BOLD C_YELLOW "3. " C_RESET "Force NetDuke32 Connection (For Troubleshooting)\n");
	colorprintf(C_BOLD C_RED "4. " C_RESET "Quit\n");

	while (1)
	{
		if (!prompt(yn, sizeof(yn)))
			continue;

		if (yn[0] == '1')
		{
			Menu_HostGameSetup();
			killme(0);
		}

		if (yn[0] == '2')
		{
			Menu_AskManualIP(false);
			goto restart_menu;
		}

		if (yn[0] == '3')
		{
			printf("NOTE: Make sure to select the same mod as the host!\n");
			Menu_AskMod();
			Menu_AskManualIP(true);
		}

		if (yn[0] == '4')
			killme(0);
	}
}

#ifdef _WIN32
typedef void(*dllSetString)(const char*);

static BOOL WINAPI CtrlHandler(DWORD fdwCtrlType)
{
	switch (fdwCtrlType)
	{
		// Handle the CTRL-C signal.
	case CTRL_C_EVENT:
		printf("Ctrl-C event\n\n");
		Beep(750, 300);
		killme(0);
		return FALSE;

		// CTRL-CLOSE: confirm that the user wants to exit.
	case CTRL_CLOSE_EVENT:
		Beep(600, 200);
		printf("Ctrl-Close event\n\n");
		killme(0);
		return FALSE;

		// Pass other signals to the next handler.
	case CTRL_BREAK_EVENT:
		Beep(900, 200);
		printf("Ctrl-Break event\n\n");
		killme(0);
		return FALSE;

	case CTRL_LOGOFF_EVENT:
		Beep(1000, 200);
		printf("Ctrl-Logoff event\n\n");
		killme(0);
		return FALSE;

	case CTRL_SHUTDOWN_EVENT:
		Beep(750, 500);
		printf("Ctrl-Shutdown event\n\n");
		killme(0);
		return FALSE;

	default:
		return FALSE;
	}
}

#endif

int main(int argc, char** argv)
{
	GetThisPath(path, MAX_PATH);
	chdir(path);

	// FIXME - Alternative to SetConsoleTitle for Linux?
#ifdef _WIN32
	SetUnhandledExceptionFilter(unhandled_handler);
	SetConsoleTitle(APPLICATION_NAME);

#ifdef _DEBUG
	HMODULE ebacktrace = LoadLibraryA("ebacktrace1.dll");
	if (ebacktrace)
	{
		dllSetString SetTechnicalName = (dllSetString)(void(*)(void))GetProcAddress(ebacktrace, "SetTechnicalName");
		dllSetString SetProperName = (dllSetString)(void(*)(void))GetProcAddress(ebacktrace, "SetProperName");

		if (SetTechnicalName)
			SetTechnicalName(APPLICATION_SHORTNAME);

		if (SetProperName)
			SetProperName(APPLICATION_NAME);
	}
#endif

	// --- URL HANDLER ---
	HKEY hKey;
	if (RegCreateKeyEx(HKEY_CURRENT_USER, "SOFTWARE\\Classes\\netlaunch", 0, NULL, REG_OPTION_NON_VOLATILE, KEY_WRITE, NULL, &hKey, NULL) == ERROR_SUCCESS)
	{
		const char* value = "URL:Netlauncher Protocol";
		RegSetValueEx(hKey, "", 0, REG_SZ, (LPBYTE)value, strlen(value) + 1);

		value = "";
		RegSetValueEx(hKey, "URL Protocol", 0, REG_SZ, (LPBYTE)value, 0);
	}

	if (RegCreateKeyEx(HKEY_CURRENT_USER, "SOFTWARE\\Classes\\netlaunch\\shell\\open\\command", 0, NULL, REG_OPTION_NON_VOLATILE, KEY_WRITE, NULL, &hKey, NULL) == ERROR_SUCCESS)
	{
		char value[MAX_PATH];
		snprintf(value, sizeof(value), "%s\\NetLauncher.exe -url \"%1\"", path);
		RegSetValueEx(hKey, "", 0, REG_SZ, (LPBYTE)value, strlen(value) + 1);
	}
	// -------------------

	//atexit(killme);
	signal(SIGABRT, killme);
	signal(SIGINT, killme);
	signal(SIGTERM, killme);
	SetConsoleCtrlHandler(CtrlHandler, TRUE);
#endif

	if (enet_initialize() != 0)
	{
		colorprintf(C_BOLD C_RED "An error occurred while initializing ENet.\n" C_RESET);
		return 1;
	}
	else colorprintf(C_GREEN "ENet Initialized!\n" C_RESET);

	char tempname[64];
	if (get_private_profile_string("Comm Setup", "PlayerName", "", tempname, sizeof(tempname), EDUKE32_CONFIG))
		Duke_ConvertColorCodes(myClientName, tempname);
	else
	{
		printf(EDUKE32_CONFIG " not found, using system username instead. Forgot to setup NetDuke32?\n");
		DWORD size = sizeof(myClientName);
		GetUserName(myClientName, USERNAME_SIZE(size));
	}

	// Command Line Arguments
	for (int curarg = 0; curarg < argc; curarg++)
	{
		if (argv[curarg] != NULL)
		{
			if (strcasecmp(argv[curarg], "-url") == 0)
			{
				if (argv[curarg + 1] != NULL && strstr(argv[curarg + 1], "netlaunch://"))
				{
					colorprintf("Parsing URL - %s\n", argv[curarg + 1]);
					char* ip = (char*)malloc(strlen(argv[curarg + 1]));
					ip = extract_between(argv[curarg + 1], "netlaunch://", "/");

					if (ip != NULL)
					{
						ENetAddress address = getAddress(ip);
						Client_ConnectLoop(&address);
						killme(0);
						return 0;
					}
				}
				else colorprintf(C_BOLD C_RED "ERROR: Given an invalid URL! - %s\n", argv[curarg + 1]);
			}
		}
	}

	Lobby_Reset();
	Menu_Main();

	killme(0);
	return 0;
}

