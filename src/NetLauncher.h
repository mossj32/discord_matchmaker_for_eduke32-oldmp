#ifndef DISCORDLAUNCHER_H
#define DISCORDLAUNCHER_H

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h> // exit
#include <string.h>
#include <time.h>

#include <enet/enet.h>

#ifdef _WIN32
#include <windows.h>
#include <conio.h>
#include <signal.h>
#include <Shlwapi.h>
#include <direct.h>
#include <urlmon.h>
#else
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <cstdio>
#include <netdb.h>
#include <limits.h>
#include <unistd.h> // readlink
#include <fcntl.h>
#include <termios.h>
#include <sys/wait.h> // waitpid
#endif

#include "crossplat.h"
#include "usefulfuncs.h"

#define APPLICATION_ID "381610007474733076";
#define APPLICATION_NAME "NetLauncher for NetDuke32"
#define APPLICATION_SHORTNAME "NetLauncher"

#define EDUKE32_CONFIG "./netduke32.cfg"

#define GAMEPORT 23513
#define MAX_MODS_COUNT 128
#define MAXCLIENTS 16

#define BASE_PARAMETERS "-nologo -nosetup -noinstancechecking"
#define SPACER " "
#define MODS_DIR "mods"
#define MAPS_DIR "usermaps"

#ifdef _WIN32
#define EXE_PATH "start netduke32.exe"
#else
#define EXE_PATH "./netduke32"
#endif

typedef struct {
	int partySize;
	int partyMax;
	int fraglimit;
	bool starting, usingAI, connected;
	char modname[MAX_PATH];
} LobbyData;
extern LobbyData lobby;

typedef struct {
	char name[128];
	char ip[253];
	bool connected, ready;
} ClientData_t;
extern ClientData_t ClientData[MAXCLIENTS];

extern char myClientName[128];
extern int myClientIndex;

void killme(int signal);
void StartGame(const char* ip);

// Just a silly little nod to COMMIT.
static inline void PrintBanner(void)
{
	colorprintf(
		C_GREEN
		C_NOBOLD	"-----------------------------------\n"
		C_BOLD		"NETCOMMIT Device Driver Version 1.3\n"
		C_NOBOLD	"-----------------------------------\n"
		C_RESET
	);
}

static inline int32_t G_GetNextClient(int32_t pNum)
{
	for (int32_t i = pNum + 1; i < MAXCLIENTS; i++)
	{
		if (ClientData[i].connected)
			return i;
	}

	return -1;
}
#define ALL_CONNECTED(i) i = 0; i != -1; i = G_GetNextClient(i)
#define ALL_CLIENTS(iter) iter = 0; iter < MAXCLIENTS; iter++

#endif