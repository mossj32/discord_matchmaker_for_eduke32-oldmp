#ifndef NETPACKETS_H
#define NETPACKETS_H

#include "NetLauncher.h"

#define NETBUF_LENGTH 8192
#define MAX_START_PACKETS 6

#ifdef NETPACKETS_CPP
#define NETPACKETS_EXTERN
#else
#define NETPACKETS_EXTERN extern
#endif

enum NetPacket_t
{
	PACKET_SERVERINFO,
	PACKET_CLIENTINFO,
	PACKET_GAMESTART,
	PACKET_MESSAGE,
	PACKET_HEARTBEAT,
	PACKET_DISCONNECT,
	PACKET_READY,
};

enum NetChan_t
{
	CHAN_PRIMARY,
	CHAN_CLIENTINFO,
	CHAN_MESSAGE,
	CHAN_MAX,
};

enum NetStatus_t : uint32_t
{
	NETSTATUS_OK,
	NETSTATUS_CONNECTFAIL,
	NETSTATUS_FILEFAIL,
	NETSTATUS_TERMINATED,
};

NetStatus_t Net_HandlePackets(void);
void Net_SendStartGame(void);

ENetHost* Net_CreateHost(size_t maxclients);
ENetHost* Net_ConnectToHost(ENetAddress* address);

void Net_DestroyHost(void);
void Net_Kill(NetStatus_t status);

NETPACKETS_EXTERN ENetHost* local_host;
NETPACKETS_EXTERN ENetPeer* serverpeer;
NETPACKETS_EXTERN uint8_t tempnetbuf[NETBUF_LENGTH];

NETPACKETS_EXTERN bool isServer;
NETPACKETS_EXTERN bool serverquit;

#endif