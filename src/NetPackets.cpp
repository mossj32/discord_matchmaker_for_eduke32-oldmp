#define NETPACKETS_CPP
#include "NetPackets.h"

static int Net_GetIndexOfClient(ENetPeer* client)
{
	return  1 + (int)(client - local_host->peers);
}

static const char* Net_GetStatusMsg(NetStatus_t status)
{
	switch (status)
	{
		case NETSTATUS_TERMINATED:	return (C_BOLD C_GREEN	"Application Terminated"	C_RESET);
		case NETSTATUS_FILEFAIL:	return (C_BOLD C_RED	"MISSING MOD"				C_RESET);
	}

	return "Unknown Reason or Timed Out";
}

// Sent by clients - Client info
void Net_SendClientInfo()
{
	if (isServer || serverpeer == NULL)
		return;

	tempnetbuf[0] = PACKET_CLIENTINFO;
	tempnetbuf[1] = 0; // Reserved
	int l = 2;

	for (int i = 0; myClientName[i]; i++)
		tempnetbuf[l++] = myClientName[i];

	tempnetbuf[l++] = '\0';

	enet_peer_send(serverpeer, CHAN_CLIENTINFO, enet_packet_create(&tempnetbuf[0], l, ENET_PACKET_FLAG_RELIABLE));
}

// Recieved by server *AND* client - Client Info
void Net_RecieveClientInfo(uint8_t* pbuf, int32_t packbufleng, ENetEvent* event)
{
	// If server, rewrite the index before rebroadcast.
	int32_t clientIndex = (isServer) ? (pbuf[1] = Net_GetIndexOfClient(event->peer)) : pbuf[1];
	int i = 2;
	int j = 0;

	lobby.partySize++;
	ClientData[clientIndex].connected = true;

	for (; pbuf[i] && j < (int)sizeof(ClientData[clientIndex].name); i++)
	{
		ClientData[clientIndex].name[j] = pbuf[i];
		j++;
	}
	ClientData[clientIndex].name[j] = '\0';

	colorprintf("%s " C_RESET "(#%d) connected! [%d/%d]\n", ClientData[clientIndex].name, clientIndex, lobby.partySize, lobby.partyMax);

#ifdef _WIN32
	PlaySound("./nl_data/sounds/connect.wav", NULL, SND_ASYNC | SND_FILENAME);
#endif
}

void Net_SendReady()
{
	tempnetbuf[0] = PACKET_READY;
	tempnetbuf[1] = 0; // Reserved

	ClientData[myClientIndex].ready = true;

	enet_peer_send(serverpeer, CHAN_CLIENTINFO, enet_packet_create(&tempnetbuf[0], 2, ENET_PACKET_FLAG_RELIABLE));
}

void Net_RecieveReady(uint8_t* pbuf, int32_t packbufleng, ENetEvent* event)
{
	// If server, rewrite the index before rebroadcast.
	int32_t clientIndex = (isServer) ? (pbuf[1] = Net_GetIndexOfClient(event->peer)) : pbuf[1];
	ClientData[clientIndex].ready = true;

	//colorprintf("%s " C_RESET "(#%d) - All OK!\n", ClientData[clientIndex].name, clientIndex);
}

// Sent by server - Server Info
void Net_SendServerInfo(ENetPeer* client)
{
	if (!isServer)
		return;

	// Do some pointer magic to get the index.
	int clientIndex = Net_GetIndexOfClient(client);
	client->data = &ClientData[clientIndex];
	sprintf(ClientData[clientIndex].name, "Unknown Client");
	enet_address_get_host_ip(&client->address, ClientData[clientIndex].ip, sizeof(ClientData[0].ip));

	printf("A new client (#%d) connecting from %s:%u. Sending server info.\n", clientIndex, ClientData[clientIndex].ip, client->address.port);

	tempnetbuf[0] = PACKET_SERVERINFO;
	tempnetbuf[1] = clientIndex;
	tempnetbuf[2] = lobby.partySize;
	tempnetbuf[3] = lobby.partyMax;
	int l = 4;

	//  Pack player info.
	for (int32_t ALL_CONNECTED(pNum))
	{
		// This may be able to be wrapped up into a packing function.
		tempnetbuf[l++] = pNum;
		tempnetbuf[l++] = ClientData[pNum].connected;
		tempnetbuf[l++] = ClientData[pNum].ready;
		int32_t len = tempnetbuf[l++] = strlen(ClientData[pNum].name);

		for (int32_t ch = 0; ch < len; ch++)
			tempnetbuf[l++] = ClientData[pNum].name[ch];
	}

	// Pack mod name. (possibly multiple names in the future)
	{
		for (int32_t ch = 0; lobby.modname[ch]; ch++)
			tempnetbuf[l++] = lobby.modname[ch];

		tempnetbuf[l++] = '\0';
	}

	enet_peer_send(client, CHAN_PRIMARY, enet_packet_create(&tempnetbuf[0], l, ENET_PACKET_FLAG_RELIABLE));
}

// Recieved by clients - Server Info
NetStatus_t Net_RecieveServerInfo(uint8_t* pbuf, int32_t packbufleng)
{
	int i = 1;

	pbuf[0] = 'S';
	myClientIndex = pbuf[i++];
	lobby.partySize = pbuf[i++];
	lobby.partyMax = pbuf[i++];
	lobby.connected = true;

	colorprintf(C_GREEN "Getting player list...\n" C_RESET);

	// Unpack player info
	for (int32_t iter = 0; iter < lobby.partySize; iter++)
	{
		// Perhaps pack this into an unwrap function.
		int32_t pNum = pbuf[i++];
		ClientData[pNum].connected = pbuf[i++];
		ClientData[pNum].ready = pbuf[i++];
		int32_t len = pbuf[i++];

		for (int32_t ch = 0; ch < len; ch++)
			ClientData[pNum].name[ch] = pbuf[i++];

		ClientData[pNum].name[len] = '\0';

		colorprintf("%s (#%d) - %s\n", ClientData[pNum].name, pNum, (ClientData[pNum].ready) ? "Ready" : "Not Ready");
	}

	// Fill out info for self and increment party size.
	sprintf(ClientData[myClientIndex].name, "%s", myClientName);
	ClientData[myClientIndex].connected = true;
	ClientData[myClientIndex].ready = false;
	lobby.partySize++;

	// Unpack mod info
	{
		int32_t ch = 0;
		for (; pbuf[i] && ch < (int)sizeof(lobby.modname); i++)
		{
			lobby.modname[ch] = pbuf[i];
			ch++;
		}
		lobby.modname[ch] = '\0'; // Terminate string
	}

	colorprintf("\nGot response from server! (Your ID: #%d)\nHost: %s " C_RESET "(Players: %d/%d)\n\n", myClientIndex, ClientData[0].name, lobby.partySize, lobby.partyMax);

	// Check if we have the mods!
	if (lobby.modname[0] != 0)
	{
		colorprintf("Current Mod: %s\n\n", lobby.modname);

		struct stat sb;
		char modpath[MAX_PATH];
		snprintf(modpath, sizeof(modpath), MODS_DIR "/%s", lobby.modname);
		if (stat(modpath, &sb) == -1)
		{
#ifdef _WIN32
			PlaySound("./nl_data/sounds/error.wav", NULL, SND_ASYNC | SND_FILENAME);
#endif
			colorprintf(C_BOLD C_RED "ERROR! Missing %s\n" C_RESET, modpath);
			Net_Kill(NETSTATUS_FILEFAIL);
			Sleep(1000);
			return NETSTATUS_FILEFAIL;
		}
	}

	// Everything passed, so let the server know we're OK.
	Net_SendReady();

	printf("Lobby is waiting, please wait patiently for the host to launch...\n");
	return NETSTATUS_OK;
}

// Sent by server - Start Game
void Net_SendStartGame(void)
{
	if (!isServer)
		return;

	printf("Sending StartGame packets... ");

	tempnetbuf[0] = PACKET_GAMESTART;

	for (int i = 0; i < MAX_START_PACKETS; i++) // Shotgun approach. Hammer a few packets their way so they don't miss the starting gun.
	{
		enet_host_broadcast(local_host, CHAN_PRIMARY, enet_packet_create(&tempnetbuf[0], 1, ENET_PACKET_FLAG_RELIABLE));
		enet_host_flush(local_host);
		printf("%d. ", i+1);
		Sleep(200);
	}

	printf("\n");

	ENetEvent event;
	while (enet_host_service(local_host, &event, 0) > 0)
	{
		switch (event.type)
		{
		case ENET_EVENT_TYPE_RECEIVE:
			enet_packet_destroy(event.packet);
			break;
		}
	}
}

// Sent by server, recieved by clients. Sends information about who disconnected.
static void Net_BroadcastDisconnect(ENetPeer* client, NetStatus_t status)
{
	if (!isServer)
		return;

	auto const pClientData = (ClientData_t*)client->data;
	const char* status_msg = Net_GetStatusMsg(status);
	int32_t clientIndex = Net_GetIndexOfClient(client);

	if(ClientData[clientIndex].connected)
		lobby.partySize--;

	pClientData->ready = false;
	pClientData->connected = false;

	colorprintf("%s " C_RESET "disconnected. [%d/%d] (%s)\n", pClientData->name, lobby.partySize, lobby.partyMax, status_msg);

	tempnetbuf[0] = PACKET_DISCONNECT;
	tempnetbuf[1] = clientIndex;
	tempnetbuf[2] = (uint8_t)status;
	tempnetbuf[3] = lobby.partySize;
	int l = 4;

	enet_host_broadcast(local_host, CHAN_CLIENTINFO, enet_packet_create(&tempnetbuf[0], l, ENET_PACKET_FLAG_RELIABLE));

#ifdef _WIN32
	PlaySound("./nl_data/sounds/disconnect.wav", NULL, SND_ASYNC | SND_FILENAME);
#endif
}

// Only recieved by clients
void Net_RecieveDisconnect(uint8_t* pbuf, int32_t packbufleng)
{
	int i = 1;

	int32_t clientIndex = pbuf[i++];
	NetStatus_t status = (NetStatus_t)pbuf[i++];
	lobby.partySize = pbuf[i++];
	
	const char* status_msg = Net_GetStatusMsg(status);
	ClientData[clientIndex].connected = false;
	ClientData[clientIndex].ready = false;

	colorprintf("%s " C_RESET "disconnected. [%d/%d] (%s)\n", ClientData[clientIndex].name, lobby.partySize, lobby.partyMax, status_msg);

#ifdef _WIN32
	PlaySound("./nl_data/sounds/disconnect.wav", NULL, SND_ASYNC | SND_FILENAME);
#endif
}

// Packet from client. (Received by server)
void Net_ParsePacketFromClient(ENetEvent* event)
{
	uint8_t* pbuf = event->packet->data;
	int32_t packbufleng = event->packet->dataLength;

	switch (pbuf[0])
	{
	case PACKET_CLIENTINFO:
		Net_RecieveClientInfo(pbuf, packbufleng, event);
		break;
	case PACKET_READY:
		Net_RecieveReady(pbuf, packbufleng, event);
		break;
	}
}

// Packet from server. (Received by client)
NetStatus_t Net_ParsePacketFromServer(ENetEvent* event)
{
	uint8_t* pbuf = event->packet->data;
	int32_t packbufleng = event->packet->dataLength;

	switch (pbuf[0])
	{
		case PACKET_SERVERINFO:
			return Net_RecieveServerInfo(pbuf, packbufleng);
		case PACKET_CLIENTINFO:
		{
			Net_RecieveClientInfo(pbuf, packbufleng, NULL);
			break;
		}
		case PACKET_DISCONNECT:
		{
			Net_RecieveDisconnect(pbuf, packbufleng);
			break;
		}
		case PACKET_READY:
		{
			Net_RecieveReady(pbuf, packbufleng, event);
			break;
		}
		case PACKET_GAMESTART:
		{
			lobby.starting = true;
			StartGame(ClientData[0].ip);
			killme(0);
			break;
		}
	}

	return NETSTATUS_OK;
}

NetStatus_t Net_HandlePackets(void)
{
	NetStatus_t status = NETSTATUS_OK;
	ENetEvent event;
	enet_host_service(local_host, NULL, 0);

	while (enet_host_check_events(local_host, &event) > 0)
	{
		switch (event.type)
		{
			case ENET_EVENT_TYPE_CONNECT:
			{
				if (isServer)
				{
					Net_SendServerInfo(event.peer);
				}
				else
				{
					enet_address_get_host_ip(&event.peer->address, ClientData[0].ip, sizeof(ClientData[0].ip));
					printf("Connected to %s:%u.\n\n", ClientData[0].ip, event.peer->address.port);
					event.peer->data = &ClientData[0];
					Net_SendClientInfo();
				}
				break;
			}

			case ENET_EVENT_TYPE_RECEIVE:
			{
				if (isServer)
				{
					// This must ALWAYS come first, in case packets need some alteration before re-transmit.
					Net_ParsePacketFromClient(&event);

					if (event.channelID == CHAN_CLIENTINFO) // If client info, broadcast to everyone.
					{
						const ENetPacket* pak = event.packet;

						event.peer->state = ENET_PEER_STATE_DISCONNECTED; // Don't send to the person who just connected.
						enet_host_broadcast(local_host, event.channelID,
							enet_packet_create(pak->data, pak->dataLength, pak->flags & ENET_PACKET_FLAG_RELIABLE));
						event.peer->state = ENET_PEER_STATE_CONNECTED;
					}
				}
				else
				{
					status = Net_ParsePacketFromServer(&event);
				}

				/*
				int type = event.packet->data[0];
				event.packet->data[0] = 'P';
				printf("A packet type %d of length %lu containing %s was received from %s on channel %u.\n",
					type,
					event.packet->dataLength,
					static_cast<uint8_t*>(event.packet->data),
					static_cast<char*>(event.peer->data),
					event.channelID);
				*/

				enet_packet_destroy(event.packet);
				break;
			}

			case ENET_EVENT_TYPE_DISCONNECT:
			{
				if (isServer)
				{
					if (event.peer->data != NULL)
						Net_BroadcastDisconnect(event.peer, (NetStatus_t)event.data);

					event.peer->data = NULL;
					enet_peer_reset(event.peer);
				}
				else
				{
					if (lobby.connected)
					{
						printf("Server disconnected. Exiting.\n");
						serverquit = true;
						killme(0);
					}
					else
					{
						printf("Couldn't get server info...\n\n");
						return NETSTATUS_CONNECTFAIL;
					}
				}
			}
		}

		if (status != NETSTATUS_OK)
			return status;
	}

	return status;
}

void Net_DestroyHost(void)
{
	if (local_host != NULL)
	{
		enet_host_destroy(local_host);
		local_host = NULL;
		serverpeer = NULL;
	}
}

ENetHost* Net_CreateHost(size_t maxclients)
{
	ENetAddress address;
	address.host = ENET_HOST_ANY;
	address.port = GAMEPORT;

	local_host = enet_host_create(&address, maxclients, CHAN_MAX, 0, 0);

	if (local_host == NULL)
	{
		printf("An error occurred while trying to create an ENet host.\n");
		return NULL;
	}

	return local_host;
}

ENetHost* Net_ConnectToHost(ENetAddress* address)
{
	if (address == NULL)
	{
		printf("Trying to connect to NULL address!\n");
		return NULL;
	}

	address->port = GAMEPORT;

	local_host = enet_host_create(NULL, 1, CHAN_MAX, 0, 0);
	if (local_host == NULL)
	{
		printf("An error occurred while trying to create an ENet client.\n");
		return NULL;
	}

	printf("ENet client initialization successful!\n");
	printf("Attempting to connect to host...\n");
	serverpeer = enet_host_connect(local_host, address, CHAN_MAX, 0);
	if (serverpeer == NULL)
	{
		Net_DestroyHost();
		printf("Connection Failed - No available peers for initiating an ENet connection.\n");
		return NULL;
	}

	return local_host;
}

// Send disconnect packets...
void Net_Kill(NetStatus_t status)
{
	if (local_host == NULL)
		return;

	if (lobby.starting)
	{
		Net_DestroyHost();
		return;
	}

	if (!isServer && !serverquit)
	{
		if (serverpeer == NULL)
			return;

		printf("Sending disconnect packet.\n");
		ENetEvent event;
		enet_peer_disconnect(serverpeer, status);
		while (enet_host_service(local_host, &event, 1000) > 0)
		{
			switch (event.type)
			{
				case ENET_EVENT_TYPE_RECEIVE:
					enet_packet_destroy(event.packet);
					break;
				case ENET_EVENT_TYPE_DISCONNECT:
					printf("Disconnection from server succeeded.\n");
					break;
			}
		}
	}
	else if (isServer)
	{
		for (int i = 0; i < MAXCLIENTS; i++)
		{
			if (local_host->peers == NULL)
				break;

			ENetPeer* peer = &local_host->peers[i];

			if (peer == NULL || peer->host == NULL)
				continue;

			if (peer->state == ENET_PEER_STATE_DISCONNECTED || peer->state == ENET_PEER_STATE_ZOMBIE)
				continue;

			enet_peer_disconnect(peer, status);

			ENetEvent event;
			while (enet_host_service(local_host, &event, 0) > 0)
			{
				switch (event.type)
				{
					case ENET_EVENT_TYPE_RECEIVE:
						enet_packet_destroy(event.packet);
						break;
					case ENET_EVENT_TYPE_DISCONNECT:
						printf("Disconnection of client succeeded.\n");
						break;
				}
			}
		}
	}

	Net_DestroyHost();
}