#include "crossplat.h"
#include "usefulfuncs.h"
#ifndef _WIN32
#include <sys/ioctl.h>
#include <termios.h>
#include <unistd.h>
#else
#include <conio.h>
#include <windows.h>
#include <Shlwapi.h>
#endif

char* GetThisPath(char* dest, size_t destSize)
{
	if (!dest) return NULL;

#ifdef _WIN32
	DWORD length = GetModuleFileNameA(NULL, dest, destSize);

	if (MAX_PATH > destSize) return NULL;
	PathRemoveFileSpecA(dest);
#else
	if (readlink("/proc/self/exe", dest, destSize) <= 0) return NULL;
	size_t separatorPos = std::string(dest).find_last_of('/');
	if (separatorPos != std::string::npos)
		dest[separatorPos] = '\0';
#endif

	return dest;
}

char* strremove(char* str, const char* sub)
{
	char* p, * q, * r;
	if ((q = r = strstr(str, sub)) != NULL) {
		size_t len = strlen(sub);
		while ((r = strstr(p = r + len, sub)) != NULL) {
			while (p < r)
				*q++ = *p++;
		}
		while ((*q++ = *p++) != '\0')
			continue;
	}
	return str;
}

char* extract_between(const char* str, const char* p1, const char* p2)
{
	const char* i1 = strstr(str, p1);
	if (i1 != NULL)
	{
		const size_t pl1 = strlen(p1);
		const char* i2 = strstr(i1 + pl1, p2);
		if (p2 != NULL)
		{
			/* Found both markers, extract text. */
			const size_t mlen = i2 - (i1 + pl1);
			char* ret = (char*)malloc(mlen + 1);
			if (ret != NULL)
			{
				memcpy(ret, i1 + pl1, mlen);
				ret[mlen] = '\0';
				return ret;
			}
		}
	}

	return NULL;
}

int prompt(char* line, size_t size)
{
	int res;
	char* nl;
	printf("\n> ");
	fflush(stdout);
	res = fgets(line, (int)size, stdin) ? 1 : 0;
	line[size - 1] = 0;
	nl = strchr(line, '\n');
	if (nl) {
		*nl = 0;
	}
	return res;
}

void find_string(char * string, const char * pattern, char buffer[], int buflength, char stopchar)
{
	memset(buffer, 0, buflength);

	char *pos = strstr(string, pattern);

	if (NULL == pos)
	{
		//
		buffer[0] = '\0';

		return;
	}

	int i = 0;

	pos += strlen(pattern);

	while (*pos != stopchar)
	{
		buffer[i] = *pos;
		++pos;
		++i;

		if (i > buflength)
		{
			buffer[0] = '\0';
			return;
		}
	}
}

char* stristr(const char* str1, const char* str2)
{
	const char* p1 = str1;
	const char* p2 = str2;
	const char* r = *p2 == 0 ? str1 : 0;

	while (*p1 != 0 && *p2 != 0)
	{
		if (tolower((unsigned char)*p1) == tolower((unsigned char)*p2))
		{
			if (r == 0)
			{
				r = p1;
			}

			p2++;
		}
		else
		{
			p2 = str2;
			if (r != 0)
			{
				p1 = r + 1;
			}

			if (tolower((unsigned char)*p1) == tolower((unsigned char)*p2))
			{
				r = p1;
				p2++;
			}
			else
			{
				r = 0;
			}
		}

		p1++;
	}

	return *p2 == 0 ? (char*)r : 0;
}

void gotoxy(int x, int y)
{
#ifdef _WIN32
	COORD coord;
	coord.X = x;
	coord.Y = y;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);
#else
	printf("%c[%d;%df", 0x1B, y, x);
#endif
}

vec2_t getconsoledims()
{
	vec2_t dims = { 0, 0 };
#ifdef _WIN32
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi);
	dims.x = csbi.srWindow.Right - csbi.srWindow.Left + 1;
	dims.y = csbi.srWindow.Bottom - csbi.srWindow.Top + 1;
#else
	struct winsize w;
	ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);

	dims.x = w.ws_col;
	dims.y = w.ws_row;
#endif
	return dims;
}

bool isValidIpAddress(const char *ip)
{
	int num, i, len;
	char *ch;

	char st[16];
	sprintf(st, "%s", ip);
	//counting number of quads present in a given IP address
	int quadsCnt = 0;

	//printf("Split IP: \"%s\"\n", st);

	len = strlen(st);

	//  Check if the string is valid
	if (len<7 || len>15)
		return false;

	ch = strtok(st, ".");

	while (ch != NULL)
	{
		quadsCnt++;
		//printf("Quad %d is %s\n", quadsCnt, ch);

		num = 0;
		i = 0;

		//  Get the current token and convert to an integer value
		while (ch[i] != '\0')
		{
			num = num * 10;
			num = num + (ch[i] - '0');
			i++;
		}

		if (num<0 || num>255)
		{
			printf("Not a valid ip\n");
			return false;
}

		if ((quadsCnt == 1 && num == 0) || (quadsCnt == 4 && num == 0))
		{
			printf("Not a valid ip, quad: %d AND/OR quad:%d is zero\n", quadsCnt, quadsCnt);
			return false;
		}

		ch = strtok(NULL, ".");
	}

	//  Check the address string, should be n.n.n.n format
	if (quadsCnt != 4)
	{
		return false;
	}

	//  Looks like a valid IP address
	return true;
}

#define CONVERT_BOLD *out++ = '1'; *out++ = 'm'; *out++ = '\033'; *out++ = '[';
#define CONVERT_NORMAL *out++ = '0'; *out++ = 'm'; *out++ = '\033'; *out++ = '[';
// Converts EDuke32 color codes to terminal color codes.
const char* Duke_ConvertColorCodes(char* out, const char* in)
{
	char* ptr = out;

	if (*in == ' ') in++; // Kill leading space

	do
	{
		if (*in == '"') // Strip quotes
			in++;

		if (*in == '\n') // Newline
			break;

		if (*in == '^' && isdigit(*(in + 1)))
		{
			*out++ = '\033';
			*out++ = '[';

			// Move past input caret, to our number.
			in++;
			if (isdigit(*in))
			{
				char code[33];
				code[0] = *in++;
				if (isdigit(*in)) // Get second digit
				{
					code[1] = *in++;
					code[2] = '\0';
				}
				else
				{
					code[1] = '\0';
				}

				int code_int = atoi(code);
				switch (code_int)
				{
				// Blue
				case 1:
					CONVERT_BOLD
					code_int = 34;
					break;

				// Dark Blue
				case 16:
					CONVERT_NORMAL
					code_int = 34;
					break;

				// Red
				case 2:
				case 21:
					CONVERT_BOLD
					code_int = 31;
					break;

				// Dark Red
				case 10:
				case 15:
					CONVERT_NORMAL
					code_int = 31;
					break;

				// Dark Grey
				case 13: // Dark Grey (Bright Black)
					CONVERT_BOLD
					code_int = 30;
					break;
				
				// Black
				case 4:
					CONVERT_NORMAL
					code_int = 30;
					break;

				// Yellow
				case 7:
				case 23:
					CONVERT_BOLD
					code_int = 33;
					break;

				// Green
				case 6:
				case 8:
				case 17:
				case 22:
					CONVERT_BOLD
					code_int = 32;
					break;
				// Dark Green
				case 14:
					CONVERT_NORMAL
					code_int = 32;
					break;

				// Grey
				case 12:
				case 18:
				default:
					CONVERT_NORMAL
					code_int = 37;
					break;
				}

				sprintf(code, "%d", code_int);

				*out++ = code[0];
				if (code[1] != '\0')
				{
					*out++ = code[1];
				}
				*out++ = 'm';
			}
		}

		*out++ = *in++;
	} while (*in);

	*out = '\0';
	return(ptr);
}

#ifdef _WIN32
// Terminal Color to Windows Console Text Attributes.
void con_color(int color, bool bold)
{
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

	switch (color)
	{
		case 0: // Reset
			color = 7;
			break;
		case 31: // Red
			color = 4;
			break;
		case 32: // Green
			color = 2;
			break;
		case 33: // Yellow
			color = 6;
			break;
		case 34: // Blue
			color = 1;
			break;
		case 35: // Magenta
			color = 5;
			break;
		case 36: // Cyan
			color = 3;
			break;
		case 37: // White
			color = 7;
			break;
		default:
			color = 7;
			break;
	}

	if (bold)
		color += 8;

	SetConsoleTextAttribute(hConsole, color);
}

// Allows printing colored text like you can with printf on linux.
// FIXME: For some reason, it's impossible to print a percent sign.
//        neither %% or doing a string print of "%" works.
void colorprintf(const char* format, ...)
{
	const char *traverse;
	unsigned int i;
	int j;
	char *s;
	static int color;
	static bool bold;

	//Module 1: Initializing Myprintf's arguments 
	va_list arg;
	va_start(arg, format);

	for (traverse = format; *traverse != '\0'; traverse++)
	{
		while (*traverse != '%' && *traverse != '\0')
		{
			if (*traverse == '\033' && traverse[1] == '[')
			{
				if (traverse[2] == '0' && traverse[3] == 'm') // Reset
				{
					bold = false;
					color = 0;
					traverse += 3;
				}
				else if (traverse[2] == '1' && traverse[3] == 'm') // Bold
				{
					bold = true;
					traverse += 3;
				}
				else if (traverse[2] == '2' && traverse[3] == '2' && traverse[4] == 'm') // No Bold
				{
					bold = false;
					traverse += 4;
				}
				else if (traverse[2] == '3' && traverse[4] == 'm')
				{
					char code[3];
					code[0] = traverse[2];
					code[1] = traverse[3];
					code[2] = '\0';
					color = atoi(code);
					traverse += 4;
				}

				con_color(color, bold);
			}
			else
			{
				putchar(*traverse);
			}

			if (*traverse != '\0') traverse++;
		}

		if (*traverse != '\0') traverse++;

		if (*traverse == '\0') break;

		//Module 2: Fetching and executing arguments
		switch (*traverse)
		{
		case 'c': i = va_arg(arg, int);     //Fetch char argument
			putchar(i);
			break;

		case 'd': j = va_arg(arg, int);         //Fetch Decimal/Integer argument
			if (j < 0)
			{
				j = -j;
				putchar('-');
			}
			colorprintf("%s", convert(j, 10));
			break;

		case 'o': i = va_arg(arg, unsigned int); //Fetch Octal representation
			colorprintf("%s", convert(i, 8));
			break;

		case 's': s = va_arg(arg, char *);       //Fetch string
			colorprintf(s);
			break;

		case 'x': i = va_arg(arg, unsigned int); //Fetch Hexadecimal representation
			colorprintf("%s", convert(i, 16));
			break;
		}
	}

	//Module 3: Closing argument list to necessary clean-up
	va_end(arg);
}

char *convert(unsigned int num, int base)
{
	static char Representation[] = "0123456789ABCDEF";
	static char buffer[50];
	char *ptr;

	ptr = &buffer[49];
	*ptr = '\0';

	do
	{
		*--ptr = Representation[num%base];
		num /= base;
	} while (num != 0);

	return(ptr);
}

// Copies string to clipboard.
void toClipboard(const std::string& s)
{
	OpenClipboard(0);
	EmptyClipboard();
	HGLOBAL hg = GlobalAlloc(GMEM_MOVEABLE, s.size()+1);
	if (!hg) {
		CloseClipboard();
		return;
	}
	memcpy(GlobalLock(hg), s.c_str(), s.size()+1);
	GlobalUnlock(hg);
	SetClipboardData(CF_TEXT, hg);
	CloseClipboard();
	GlobalFree(hg);
}
#endif

#ifndef _WIN32
bool kbhit()
{
	termios term;
	tcgetattr(0, &term);

	termios term2 = term;
	term2.c_lflag &= ~ICANON;
	tcsetattr(0, TCSANOW, &term2);

	int byteswaiting;
	ioctl(0, FIONREAD, &byteswaiting);

	tcsetattr(0, TCSANOW, &term);

	return byteswaiting > 0;
}

char getch()
{
	termios term;
	tcgetattr(0, &term);

	termios term2 = term;
	term2.c_lflag &= ~ICANON;
	tcsetattr(0, TCSANOW, &term2);

	char ch = getchar();

	tcsetattr(0, TCSANOW, &term);

	return ch;
}
#endif