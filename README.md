# NetLauncher - A Terminal-based Matchmaker
NetLauncher is a matchmaking frontend for the Duke Nukem 3D port NetDuke32, allowing players to host and join matches through direct IP.
It supports UPnP, allowing the program to automatically forward ports for NetDuke32 if they aren't already mapped, and if the PC has SSDP/Network Discovery enabled and router has UPnP enabled.

Uses some modified code from GaryNull's "Tin Can Phone" for UPnP support: https://github.com/garynull/tincanphone

**Future Plans:**
 - Support for NBlood (Blood source port)
 - Master Server support and server listings
 - Lobby chat

**Compiling NetLauncher:**
Compiling NetLauncher should be very simple. For Windows, either Visual Studio 2019 or later, or MSYS2 MinGW 32-bit or 64-bit should do the job. On Linux, as long as you have the GCC C++ toolchain and make, you should be good to go.
