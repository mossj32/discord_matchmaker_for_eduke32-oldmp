=====================================================
Title			: Pain
Filename		: PAIN.MAP
Author			: Stranger
Email address		: None at this time

Description		: A dukematch arena with exquisite architecture
			  and detail. This map is the pure manifestation
                          of pain - explore it and rediscover the hate
			  that lingers beneath your skin... 

Additional credits to   : No one
Greets go to		: Bitoku, Insomniac, Reznor, Radon, Useless,
			  Goldeneye and Tomrox10.
=====================================================

* Play information *

Episode and level#      : Default
			  (Note: Plutonium pak/atomic edition required to
				 play this map)
			  (Suggested music to switch to:
			   Doom64's music from level 20: breakdown
			   Doom64's music from level 31: the void)
Single player		: No
Dukematch level		: Yes
Co-op play		: No
Difficulty settings	: Not implemented

* Construction *

Base			: New level from scratch
Editor used		: BUILD v. 1.4
Known bugs              : None.

* Notes and any other stuff that hasn't been covered *

Please don't be alarmed by the fact that there are no items when exploring
in single player, because all of the items are tagged as dukematch only
and will only appear in a net/modem game. I have removed them for exploring
purposes so you could look at the architecture without any items annoying
you.

You MUST have atomic edition or the plutonium pak to run this map. If you
don't, the map will crash as soon as you boot it up.

It is *HIGHLY* recommended you use the con file provided with pain.map.
This eliminates the 'blackout effect' which is annoying and not needed.
I also suggest you use this con with every map you play, because it
will not conflict with default con files (you will not get out of sync).
It changes the parameter "default visibility" from 512 to 8... Just so
you can change it back manually if you really need to.


----- STORY -----

Something is wrong.

Something is approaching but I can't run away.

I can't fight back. It washes over me in waves. It makes my eyes bleed.
It makes my knuckles white.
This crimson fluid is covering me, coating me, burning me. The burning seeps
through my skin. It enters my veins and I scream. It infects my blood.
My insides ignite.

My hands solidify as it encases them; my veins boil with this intruding
energy - and a sound repeats itself over and over. The sound is contained
within my head. Others around me hear nothing.

This righteous fire, this unholy power - taking me bit by bit. Eating me.
Consuming me. This crimson fluid - this poison - changing me.
It draws deeper and deeper into me and my mouth opens; attempting a scream
of despair. I hear nothing. I try to cry but my eyes shed no tears.
I thrash my arms but I do not move.

As time travels past, this thing - this horrible thing - encases my body.
It replaces my blood with tainted fluid which bubbles through into my brain.
My brain at first screams its own psychic scream - then is silent.

This sound - this repeating horrible noise - tampering with my thoughts -
I beg it to stop but it wont give up. I try not to listen but its all too
clear. I laugh. When I laugh it echoes agony, insanity, and rage. Why won't
it stop...

Where is it taking me? It forces my eyes shut. It's blinding me. I still
feel its bitter wrath rampaging through my bloodstream and my mind goes
fuzzy.

I fall.

I feel something cold against my cheek. It is... horizontal... flat...
cold. Its the floor, a cold stone floor. My mind is beginning to clear up
and the first question I ask is "Where am I?". 

My senses spring back to life. My hands are no longer petrified and I can
feel them, full of new energy. My ears come back and I can hear the sound
of my breathing, laboured, uneven breathing. My ears hear nothing else. The
noise - that horrible noise - is slowly fading away.

It seems everything is returning but - something is still wrong. That thing
has corrupted my mind, corrupted my thoughts! Something is very, very wrong.

"Where am I?" I ask again.

Wait. Something is happening. I know where I am. I give a little laugh.
I know where I am... 

My eyes suddenly open wide and I realize there is something in my hand.

It's a gun.

Without my consent, my mouth twists into a little smile.
Theres someone else here. Yes, most definately someone else is here...
Let us see how corrupted my mind is, after all.

"Let us burn together..."

"Feel my PAIN!!!"


PTL
